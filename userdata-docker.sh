#!/bin/bash
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install docker.io git -y
sudo usermod -aG docker ubuntu
sudo reboot
# -----> after execute instance
docker --version
# -----> docker ok
docker pull nginx
# -----> get nginx image
docker images
# -----> nginx image ok
docker run --name some-nginx -d -p 8080:80 nginx
# -----> run docker nginx image
docker ps
# -----> run ok
docker rm -f some-nginx
# -----> docker stoped and removed
docker ps
# -----> stoped ok
git clone https://gitlab.com/mcardosoabreu/devops_cloud_week.git
# -----> charge the project in nginx instance
ls
cd devops_cloud_week
ls
# -----> clone ok