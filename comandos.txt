/* Comandos Docker */

docker images         # Visualizar as imagens docker disponiveis.
docker ps             # Visualizar os containers rodando atualmente.
docker run            # Rodar um container, necessário passar os parametros de uso.
docker build          # Faz o build de uma imagem docker baseado no Dockerfile que deve ser informado via parametros.
docker rm             # Apaga um container que não está mais sendo utilizado, pode-se utiliza-lo junto ao -f para forçar a parada de um container em execução. 
docker rmi            # Apaga uma imagem armazenada localmente.


/* Comando utilizados durante a live */

docker build -t dcw-app .            # Irá criar sua propria imagem baseado em instruções no dockerfile.
docker run -itd -p 80:3000 dcw-app   # Irá rodar o container com a imagem previamente criada e expondo a aplicação na porta 80
docker ps                            # Verificar se o container está rodando.

/* Após criar repositório no DockerHub */
docker images
docker tag dcw-app mcardosoabreu/dcw-app
docker images

/* Mandar docker para o dockerhub */
docker login
docker push mcardosoabreu/dcw-app

/* Rodar essa imagem criada em outra máquina */
docker run -itd -p 80:3000 devopscloudweek22/dcp-app 